# README #

https://www.magentocommerce.com/magento-connect/facebook-pixel-standard-events.html
![](README/facebook-pixel-standard-events.png)
仅简单地提交了 event，却没含具体的参数，要就得装其收费的
https://www.magentocommerce.com/magento-connect/facebook-pixel-for-remarketing.html

而这些 Facebook 官方提供的插件一并提供的README了



看一下 /etc/system.xml，学习其 comment 的使用	

---

# Reference #

https://github.com/r-martins/Magento1-FacebookPixelImprovements
增加了参数



https://github.com/Turiknox/magento-facebook-pixel
在 Observer 内就将具体的参数整好了，放 session 待取
system.xml 中的 field frontend_type 为 multiselect 时，通过 getStoreConfig( ) 返回的是一个逗号分隔的 string
具体使用如下：
```
    public function isEventAllowed($event)
    {
        $_events = explode(',', Mage::getStoreConfig('facebookpixel/integration/events'));

        foreach ($_events as $_event) {
            if ($event === $_event) {
                return true;
            }
        }
        return false;
    }
```
