<?php
/**
 * 至此终于明白了，session 被当作全局变量来使用
 * 通过在 session 所做的标记，就无需通过当前所在页面来判断，
 * 也就无需在 layout.xml 中找相应的 controller action 的 handle 在其内加 block 来输出
 */
class Remmote_Facebookpixel_Model_Observer
{
    /**
     * Set a flag in session when a product is added to the cart
     * @param  [type]     $observer
     * @return [type]
     */
    /**
     * 这个事件重要的仅有一个，即 product id，数量不要求知道
     *
     * 因为这个动作不会有转到新的页面，所以仅能通过 dispatchEvent 传出来的
     */
    public function logPixelAddToCart($observer) {
        //Logging event
        // 因为仅是作为标记使用，类比于全局变量，真正使用后必须清空以免影响下次的判断
        // set 也就是 write 的时候为了防止 lock 不是应该用 getSingleton 的么？
        Mage::getModel('core/session')->setPixelAddToCart(true);


        $product = $observer->getProduct();
        // 你倾向于这种写法，在 session 中放置的直接就是数组，这样取出来就能用
        $addToCartData = array();
        $addToCartData['content_type'] = 'product';
        $addToCartData['content_ids'] = $product->getSku();
        $addToCartData['content_name'] = $product->getName();
        // 此处若不做判断则会:
        // Fatal error: Call to a member function getName() on a non-object
        if( $product->getCategory() ){
            $addToCartData['content_category'] = $product->getCategory()->getName();
        }
        $addToCartData['value'] = $product->getFinalPrice();
        $addToCartData['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();
        Mage::getSingleton('core/session')->setData('fb_addtocart', $addToCartData);

    }

    /**
     * Set a flag in session when a product is added to the wishlist
     * @param  [type]     $observer
     * @return [type]
     */
    public function logPixelAddToWishlist($observer) {
        //Logging event
        Mage::getModel('core/session')->setPixelAddToWishlist(true);


        $product = $observer->getProduct();

        $addToWishlistData = array();
        $addToWishlistData['content_type'] = 'product';
        $addToWishlistData['content_ids'] = $product->getSku();
        $addToWishlistData['content_name'] = $product->getName();
        if( $product->getCategory() ){
            $addToCartData['content_category'] = $product->getCategory()->getName();
        }
        $addToWishlistData['value'] = $product->getFinalPrice();
        $addToWishlistData['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();

        Mage::getSingleton('core/session')->setData('fb_addtowishlist', $addToWishlistData);
    }

    /**
     * Set a flag in session when a customer selects the payment method
     * @param  [type]     $observer
     * @return [type]
     */
    public function logPixelPaymentInfo($observer){
        //Logging event
        Mage::getModel('core/session')->setPixelPaymentInfo(true);
    }

    /**
     * Set a flag in session when a purchase is made
     * @param  [type]     $observer
     * @return [type]
     */
    public function logPixelPurchase($observer){
        //Logging event
        Mage::getModel('core/session')->setPixelPurchase(true);


        $order = $observer->getOrder();
        $numItems = $order->getAllVisibleItems();

        $itemSkus = array();
        foreach ($numItems as $item) {
            $itemSkus[] = htmlspecialchars($item->getSku());
        }

        $purchaseData = array();
        $purchaseData['value'] = number_format($order->getGrandTotal(), 2);
        $purchaseData['num_items'] = count($numItems);
        $purchaseData['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();
        $purchaseData['content_type'] = 'product';
        $purchaseData['content_ids'] = $itemSkus;

        Mage::getSingleton('core/session')->setData('fb_purchase', $purchaseData);
    }

    /**
     * Set a flag in session when a customer creates an account
     * @param  [type]     $observer
     * @return [type]
     */
    public function logPixelCompleteRegistration($observer){
        //Logging event
        Mage::getModel('core/session')->setPixelCompleteRegistration(true);


        $customer = $observer->getCustomer();

        $confirmation = 'not_confirmed';
        if (!$customer->getConfirmation()) {
            $confirmation = 'confirmed';
        }

        $completeRegistrationData = array();
        $completeRegistrationData['status'] = $confirmation;

        Mage::getSingleton('core/session')->setData('fb_completeregistration', $completeRegistrationData);
    }

    /**
     * Set a flag in session when a customer signup to the newsletter
     * @param  [type]     $observer
     * @return [type]
     */
    public function logPixelCompleteRegistrationNewsletter($observer) {
        $subscriber     = $observer->getEvent()->getSubscriber();
        $statusChanged  = $subscriber->getIsStatusChanged();
        if($subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED && $statusChanged) {
            Mage::getModel('core/session')->setPixelCompleteRegistration(true);

            //Set flag for Lead Event
            Mage::getModel('core/session')->setPixelLead(true);
        }
    }


    
    /**
     * 这下面的代码都不适合出现在 Observer 中
     */
    /**
     * Set data when customer goes to the onepage checkout page
     *
     * @return $this
     */
    public function setInitiateCheckout()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $numItems = $quote->getAllVisibleItems();

        $itemSkus = array();
        foreach ($numItems as $item) {
            $itemSkus[] = htmlspecialchars($item->getSku());
        }

        $initiateCheckoutData = array();
        $initiateCheckoutData['value'] = number_format($quote->getSubtotal(), 2);
        $initiateCheckoutData['num_items'] = count($numItems);
        $initiateCheckoutData['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();
        $initiateCheckoutData['content_type'] = 'product';
        $initiateCheckoutData['content_ids'] = $itemSkus;

        Mage::getSingleton('core/session')->setData('fb_initiatecheckout', $initiateCheckoutData);
        return $this;
    }

    /**
     * If One Step Checkout is enabled, head to initiateCheckout()
     *
     * @return $this
     */
    public function setInitiateIdevOsc()
    {
        if ($this->isCheckoutModuleEnabled('Idev_OneStepCheckout')) {
            // Additional check if the module is enabled in the admin
            if (Mage::helper('onestepcheckout')->isRewriteCheckoutLinksEnabled()) {
                $this->setInitiateCheckout();
            }
        }
        return $this;
    }

    /**
     * If IWD One Page Checkout is enabled, head to initiateCheckout()
     *
     * @return $this
     */
    public function setInitiateIwdOpc()
    {
        if ($this->isCheckoutModuleEnabled('IWD_Opc')) {
            // Additional check if the module is enabled in the admin
            if (Mage::helper('opc')->isEnable()) {
                $this->setInitiateCheckout();
            }
        }
        return $this;
    }
}